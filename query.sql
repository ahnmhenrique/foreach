CREATE TABLE usuarios (
	id_usuario		SERIAL		NOT NULL UNIQUE,
	nome			TEXT		NOT NULL,
	email			TEXT		NOT NULL UNIQUE,
	senha			TEXT		NOT NULL,
	
	PRIMARY KEY (id_usuario)
);

CREATE TABLE cursos (
	id_curso		SERIAL		NOT NULL UNIQUE,
	nome			TEXT		NOT NULL UNIQUE,
	sigla			TEXT		NOT NULL UNIQUE,
	
	PRIMARY KEY (id_curso)
);

INSERT INTO cursos (nome, sigla) VALUES ('Ci�ncias da Ativadade F�sica', 'CAF');
INSERT INTO cursos (nome, sigla) VALUES ('Licenciatura em Ci�ncias da Natureza', 'LCN');
INSERT INTO cursos (nome, sigla) VALUES ('Gerontologia', 'GERONTO');
INSERT INTO cursos (nome, sigla) VALUES ('Gest�o Ambiental', 'GA');
INSERT INTO cursos (nome, sigla) VALUES ('Gest�o de Pol�ticas P�blicas', 'GPP');
INSERT INTO cursos (nome, sigla) VALUES ('Lazer e Turismo', 'LZT');
INSERT INTO cursos (nome, sigla) VALUES ('Marketing', 'MKT');
INSERT INTO cursos (nome, sigla) VALUES ('Obstetr�cia', 'OBS');
INSERT INTO cursos (nome, sigla) VALUES ('Sistemas de Informa��o', 'SI');
INSERT INTO cursos (nome, sigla) VALUES ('T�xtil e Moda', 'TM');

CREATE TABLE materias (
	id_materia		SERIAL		NOT NULL UNIQUE,
	codigo			TEXT		NOT NULL UNIQUE,
	nome			TEXT		NOT NULL,
	semestre		SMALLINT	NOT NULL,
	
	PRIMARY KEY (id_materia)
);

INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0011', 'Ci�ncias da Natureza', 1);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0021', 'Tratamento e An�lise de Dados/ Informa��es', 1);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0031', 'Sociedade, Multiculturalismo e Direitos', 1);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0041', 'Resolu��o de Problemas I', 1);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0051', 'Estudos Diversificados I', 1);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0012', 'Psicologia, Educa��o e Temas Contempor�neos', 2);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0022', 'Sociedade, Meio Ambiente e Cidadania', 2);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0032', 'Arte, Literatura e Cultura no Brasil', 2);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0042', 'Resolu��o de Problemas II', 2);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH0052', 'Estudos Diversificados II', 2);

INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH2001', 'Introdu��o � Ci�ncia da Computa��o I', 1);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH2011', 'C�lculo I', 1);

INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH2002', 'Introdu��o � Ci�ncia da Computa��o II', 2);
INSERT INTO materias (codigo, nome, semestre) VALUES ('ACH2012', 'C�lculo II', 2);

CREATE TABLE materias_cursos (
	id_materia		INTEGER		NOT NULL,
	id_curso		INTEGER		NOT NULL,
	
	PRIMARY KEY(id_materia, id_curso),
	
	CONSTRAINT id_materia_fk FOREIGN KEY (id_materia) REFERENCES materias (id_materia),
	CONSTRAINT id_curso_fk FOREIGN KEY (id_curso) REFERENCES cursos (id_curso)
);

INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (1, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (2, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (3, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (4, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (5, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (6, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (7, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (8, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (9, 10);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 1);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 2);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 3);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 4);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 5);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 6);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 7);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 8);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (10, 10);

INSERT INTO materias_cursos (id_materia, id_curso) VALUES (11, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (12, 9);

INSERT INTO materias_cursos (id_materia, id_curso) VALUES (13, 9);
INSERT INTO materias_cursos (id_materia, id_curso) VALUES (14, 9);

CREATE TABLE turmas (
	id_turma		SERIAL		NOT NULL UNIQUE,
	id_materia		INTEGER		NOT NULL,
	num_turma		SMALLINT	NOT NULL,
	ano				SMALLINT	NOT NULL,
	semestre		SMALLINT	NOT NULL,
	num_posts		INTEGER		DEFAULT 0,
	
	PRIMARY KEY (id_turma),
	CONSTRAINT id_materia_fk FOREIGN KEY (id_materia) REFERENCES materias (id_materia)
);

CREATE TABLE favoritas (
	id_favorita		SERIAL		NOT NULL UNIQUE,
	id_turma		INTEGER		NOT NULL,
	id_usuario		INTEGER		NOT NULL,
	
	PRIMARY KEY (id_favorita),
	CONSTRAINT id_turma_fk FOREIGN KEY (id_turma) REFERENCES turmas (id_turma),
	CONSTRAINT id_usuario_fk FOREIGN KEY (id_usuario) REFERENCES usuarios (id_usuario)
);

CREATE TABLE posts (
	id_post			SERIAL		NOT NULL UNIQUE,
	id_turma		INTEGER		NOT NULL,
	id_usuario		INTEGER		NOT NULL,
	data_postagem	TIMESTAMP	NOT NULL,
	titulo			TEXT		NOT NULL,
	conteudo		TEXT		NOT NULL,
	visualizacoes	INTEGER		DEFAULT 0,
	comentarios		INTEGER		DEFAULT 0,
	link			TEXT		NOT NULL UNIQUE,
	
	PRIMARY KEY (id_post),
	CONSTRAINT id_turma_fk FOREIGN KEY (id_turma) REFERENCES turmas (id_turma),
	CONSTRAINT id_usuario_fk FOREIGN KEY (id_usuario) REFERENCES usuarios (id_usuario)
);

CREATE TABLE comentarios (
	id_comentario	SERIAL		NOT NULL UNIQUE,
	id_post			INTEGER		NOT NULL,
	id_usuario		INTEGER		NOT NULL,
	comentario		TEXT		NOT NULL,
	
	PRIMARY KEY (id_comentario),
	CONSTRAINT id_post_fk FOREIGN KEY (id_post) REFERENCES posts (id_post),
	CONSTRAINT id_usuario_fk FOREIGN KEY (id_usuario) REFERENCES usuarios (id_usuario)
);

/* Senha = teste123 */
INSERT INTO usuarios (nome, email, senha) VALUES ('Teste EACH', 'teste', '50f6f5ef83e2c28fb0f47d466c0c8d3a');

INSERT INTO turmas (id_materia, num_turma, ano, semestre, num_posts) VALUES (11, 02, 2013, 1, 2);
INSERT INTO turmas (id_materia, num_turma, ano, semestre) VALUES (11, 04, 2013, 1);
INSERT INTO turmas (id_materia, num_turma, ano, semestre) VALUES (11, 94, 2013, 1);
INSERT INTO turmas (id_materia, num_turma, ano, semestre) VALUES (12, 02, 2013, 1);
INSERT INTO turmas (id_materia, num_turma, ano, semestre) VALUES (12, 04, 2013, 1);
INSERT INTO turmas (id_materia, num_turma, ano, semestre) VALUES (12, 94, 2013, 1);

INSERT INTO favoritas (id_turma, id_usuario) VALUES (1, 1);
INSERT INTO favoritas (id_turma, id_usuario) VALUES (4, 1);

INSERT INTO posts (id_turma, id_usuario, data_postagem, titulo, conteudo, link) VALUES (1, 1, '2012-11-10 10:23:54', 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, esse indoctum eu nec. Usu porro libris indoctum an, pri an vidisse eruditi. Duo placerat vivendum intellegebat et, pri in noster vocibus. Amet mnesarchum ea mea, qui at modus error timeam. Ne eos feugait interpretaris, vis in possim adversarium, erant tation ad cum.', 'lorem-ipsum');
INSERT INTO posts (id_turma, id_usuario, data_postagem, titulo, conteudo, link) VALUES (1, 1, '2012-11-11 10:23:54', 'Porque usamos?', '� um fato conhecido de todos que um leitor se distrair� com o conte�do de texto leg�vel de uma p�gina quando estiver examinando sua diagrama��o. A vantagem de usar Lorem Ipsum � que ele tem uma distribui��o normal de letras, ao contr�rio de "Conte�do aqui, conte�do aqui", fazendo com que ele tenha uma apar�ncia similar a de um texto leg�vel.', 'porque-usamos');