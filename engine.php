<?php

	// Fonte: http://www.postgresql.org/docs/8.1/static/errcodes-appendix.html
	$ERROR_UNIQUE_VIOLATION = 23505;
	
	function toAscii($str) {
		setlocale(LC_ALL, "pt_BR", "ptb");
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		return preg_replace("/[\/_|+ -]+/", "-", $clean);
	}
	
	function displayBackButton() {
		echo "<a href=\"javascript: history.back();\" id=\"back_button\">Voltar</a> ";
	}
	
	function displayFavButton($id_usuario, $num_turma, $codigo_materia) {
		global $dbcon;
		
		$query = sprintf("SELECT id_turma FROM turmas INNER JOIN materias ON codigo='%s'
			AND turmas.id_materia=materias.id_materia AND num_turma=%d ", $codigo_materia, $num_turma);
		
		$result = pg_query($dbcon, $query);
		
		if(pg_num_rows($result)) {
		
			$row = pg_fetch_array($result);
			
			$id_turma = $row["id_turma"];
			
			$query = sprintf("SELECT id_favorita FROM favoritas  WHERE id_usuario=$id_usuario AND favoritas.id_turma=%d", $id_turma);
			
			$result = pg_query($dbcon, $query);
			
			if(pg_num_rows($result) == 0)
			{
				echo "<input onClick=\"favoritar($id_usuario, $id_turma);\" value=\"\" type=\"button\" id=\"favoritar\" class=\"fav_button\"/>";
			}
			else
			{
				echo "<input onClick=\"desfavoritar($id_usuario, $id_turma);\" value=\"\" type=\"button\" id=\"desfavoritar\" class=\"fav_button\"/>";
			}
		}
	}
	
	function getNomeMateria($codigo) {
	
		global $dbcon;
		$result = pg_query($dbcon, "SELECT nome FROM materias WHERE codigo='$codigo'");
		
		if(pg_num_rows($result)) {
			
			$row = pg_fetch_array($result);
			
			return $row["nome"];
		}
		
		return "";
	}
	
	function getTileColor($index) {
	
		$index = $index % 10;
		
		switch ($index) {
		
			case 0:
				return "#f47421";
				
			case 1:
				return "#164089";
				
			case 2:
				return "#00cf00";
				
			case 3:
				return "#04aeda";
				
			case 4:
				return "#c40000";			
				
			case 5:
				return "#009f3c";
				
			case 6:
				return "#2b76cd";
				
			case 7:
				return "#0e5d30";
				
			case 8:
				return "#642D64";	
				
			case 9:
				return "#00a9ec";
		
			default:
				break;
		}
		
		return "#000000";
	}

	if (!isset($_POST["action"])) {
		echo "Get the FUCK out. :D";
	}
	else {
		$dbcon = pg_connect("host=localhost port=5432 dbname=foreach user=postgres password=root");
		if (!$dbcon) {
			die("Error!");
		}
		
		$action = $_POST["action"];
		if ($action == "logout") {
			$exp = time() - 3600;
			setcookie("email", "", $exp);
			setcookie("senha", "", $exp);
			setcookie("id_usuario", "", $exp);
		}
		else {
			$isloged = false;
			if (isset($_COOKIE["email"]) && isset($_COOKIE["senha"]) && isset($_COOKIE["id_usuario"])) {
				$email = addslashes($_COOKIE["email"]);
				$senha = addslashes($_COOKIE["senha"]);
				
				$result = pg_query($dbcon, "SELECT id_usuario FROM usuarios WHERE email='$email' AND senha='$senha'");
				if(pg_num_rows($result) == 1) {
					$exp = time() + (60 * 60 * 24 * 30);
					setcookie("email", $email, $exp);
					setcookie("senha", $senha, $exp);
					$isloged = true;
					$row = pg_fetch_array($result);
					setcookie("id_usuario", $row["id_usuario"], $exp);
					$userid = $row["id_usuario"];
				}
			}
			
			
			switch($action) {
				case "login":
					// if (isset($_POST["email"]) && isset($_POST["senha"])) {
						// $email = addslashes($_POST["email"]);
						// $senha = md5("G&DSytqw" . $_POST["senha"] . "83he2dSHA");
						
						// $result = pg_query($dbcon, "SELECT id_usuario FROM usuarios WHERE email='$email' AND senha='$senha'");
						// if(pg_num_rows($result) == 1) {
							// $exp = time() + (60 * 60 * 24 * 30);
							// setcookie("email", $email, $exp);
							// setcookie("senha", $senha, $exp);
							// $row = pg_fetch_array($result);
							// setcookie("id_usuario", $row["id_usuario"], $exp);
							// echo "yes";
						// }
						// else {
							// echo "no";
						// }
					// }
					echo 'yes';
					break;
					
				case "register":
					if (isset($_POST["nome"]) && isset($_POST["email"]) && isset($_POST["senha"])) {
						$nome = addslashes($_POST["nome"]);
						$email = addslashes($_POST["email"]);
						$result = pg_query($dbcon, "SELECT id_usuario FROM usuarios WHERE email='$email'");
						if(pg_num_rows($result) == 0) {
							$senha = md5("G&DSytqw" . $_POST["senha"] . "83he2dSHA");
							$result = pg_query($dbcon, "INSERT INTO usuarios (nome, email, senha) VALUES ('$nome', '$email', '$senha')");
							echo "yes";
						}
						else {
							echo "no";
						}
					}
					break;
					
				case "request_favoritas":
					if ($isloged) {
					
						displayBackButton();
						
						$result = pg_query($dbcon, "SELECT codigo, nome, ano, turmas.semestre, num_turma FROM turmas, materias, favoritas WHERE favoritas.id_usuario=$userid AND turmas.id_turma=favoritas.id_turma AND turmas.id_materia=materias.id_materia");
						
						echo "<div class=\"lista\">";
						if (pg_num_rows($result) == 0) {
							echo "Voc&ecirc; ainda n&atilde;o possui nenhuma disciplina favorita.";
						}
						else {
							echo "<ul id=\"materias_fav\">";
							while (($row = pg_fetch_array($result))) {
								echo sprintf("<li><a href=\"#turma/%d/%d/%'02d/%s\"><h3>%s</h3><h1>T%'02d</h1><h2>%s</h2></a></li>", $row["ano"], $row["semestre"], $row["num_turma"], $row["codigo"], $row["codigo"], $row["num_turma"], $row["nome"]);
							}
							echo "</ul>";
						}
						echo "</div>";
					}
					break;
					
				case "request_cursos":
					if ($isloged) {
					
						echo "<a id=\"novo_curso\" href=\"#novocurso\">Novo curso</a>";
						
						$result = pg_query($dbcon, "SELECT nome, sigla FROM cursos");
						echo "<div class=\"lista\"><ul class=\"materias_cursos\" id=\"materias_cursos\">";
						$i = 0;
						while (($row = pg_fetch_array($result))) {
							$color = getTileColor($i);
							
							echo sprintf("<li id=\"%s\" style=\"background-color: $color;\" ><a href=\"#cursos/%s\"><h2>%s</h2></a></li>", strtolower($row['sigla']), $row['sigla'], $row['nome']);
							$i++;
						}
						echo "</ul></div>";
					}
					break;
					
				case "request_materias":
					if ($isloged && isset($_POST["sigla"])) {
						displayBackButton();
						
						$sigla = $_POST["sigla"];
						echo "<a id=\"adicionar_materia\" href=\"#addmateria/$sigla\">Adicionar mat&eacute;rias</a>";
						
						$result = pg_query($dbcon, sprintf("SELECT cursos.nome FROM cursos WHERE sigla='%s'", addslashes($_POST["sigla"])));
						$row = pg_fetch_array($result);						
						
						echo "<h1>" . $_POST["sigla"] . " - " . $row["nome"] . "</h1>";
						
						for($i = 1; $i <= 8; $i++) {
							
							$query = sprintf("SELECT materias.codigo, materias.nome FROM materias, materias_cursos, cursos WHERE materias.semestre=$i AND sigla='%s' AND cursos.id_curso=materias_cursos.id_curso AND materias.id_materia=materias_cursos.id_materia", addslashes($_POST["sigla"]));					
							$result = pg_query($dbcon, $query);
							
							echo "<table class=\"materias_curso\"><tr><th colspan=\"2\">" . $i . "&ordm; Per&iacute;odo Ideal</th></tr>";
							while (($row = pg_fetch_array($result))) {
								echo sprintf("<tr><td class=\"codigo\"><a href=\"#cursos/%s/%s\">%s</td><td>%s</a></td></tr>", $_POST["sigla"], $row["codigo"], $row["codigo"], $row["nome"]);
							}
							echo "</table>";
						}
					}
					break;
					
				case "request_turmas_materia":
					if ($isloged && isset($_POST["sigla"]) && isset($_POST["codigo"])) {
					
						displayBackButton();
						
						$sigla = $_POST["sigla"];
						$codigo = $_POST["codigo"];
						
						echo "<a id=\"nova_turma\" href=\"#novaturma/$sigla/$codigo\">Nova turma</a>";
						
						$materia = getNomeMateria($codigo);
						
						echo "<h1>$materia - " . $_POST["codigo"] . "</h1>";
					
						$result = pg_query($dbcon, sprintf("SELECT codigo, num_posts, ano, turmas.semestre, num_turma FROM turmas, materias, materias_cursos, cursos WHERE cursos.sigla='%s' AND materias.codigo='%s' AND cursos.id_curso=materias_cursos.id_curso AND materias.id_materia=materias_cursos.id_materia AND turmas.id_materia=materias.id_materia", addslashes($_POST["sigla"]), addslashes($_POST["codigo"])));
						
						echo "<div class=\"lista\">";
						if (pg_num_rows($result) == 0) {
							echo "Esta mat&eacute;ria n&atilde;o possui turmas ainda. Seja o primeiro a criar uma!";
						}
						else {
							echo "<ul id=\"materias_fav\">";
							while (($row = pg_fetch_array($result))) {
								echo sprintf("<li><a href=\"#turma/%d/%d/%'02d/%s\"><h1>T%'02d</h1><h2>%d posts</h2></a></li>", $row["ano"], $row["semestre"], $row["num_turma"], $row["codigo"], $row["num_turma"], $row["num_posts"]);
							}
							echo "</ul>";
						}
						echo "</div>";
					}
					break;
					
				case "request_post":
					if ($isloged && isset($_POST["link"]) && isset($_POST["codigo"]) && is_numeric($_POST["turma"]) && is_numeric($_POST["ano"]) && is_numeric($_POST["semestre"])) {
					
						displayBackButton();
						
						$result = pg_query($dbcon, sprintf("SELECT id_post, usuarios.nome, data_postagem, titulo, conteudo FROM usuarios, posts, turmas, materias WHERE num_turma=%d AND ano=%d AND turmas.semestre=%d AND posts.link='%s' AND materias.id_materia=turmas.id_materia  AND posts.id_turma=turmas.id_turma AND usuarios.id_usuario=posts.id_usuario", $_POST["turma"], $_POST["ano"], $_POST["semestre"], addslashes($_POST["link"])));
						$row = pg_fetch_array($result);
						
						echo sprintf("<div id=\"post\"><h1>%s</h1><span>&raquo; por <b>%s</b> em <b>%s</b></span><p>%s</p></div>", $row["titulo"], $row["nome"], date("d/m/Y", strtotime($row["data_postagem"])), $row["conteudo"]); 
						pg_query($dbcon, sprintf("UPDATE posts SET visualizacoes=visualizacoes+1 WHERE id_post=%d", $row["id_post"]));
					}
					break;
					
				case "request_turma":
					if ($isloged && isset($_POST["codigo"]) && is_numeric($_POST["turma"]) && is_numeric($_POST["ano"]) && is_numeric($_POST["semestre"])) {
					
						$materia = getNomeMateria($_POST["codigo"]);
						
						echo "<h1>$materia - " . $_POST["codigo"] . " - Turma " . $_POST["turma"];
						displayFavButton($_COOKIE["id_usuario"], $_POST["turma"], $_POST["codigo"]);
						echo "</h1>";
						
						displayBackButton();						
					
						$result = pg_query($dbcon, sprintf("SELECT data_postagem, usuarios.nome, titulo, link, visualizacoes FROM materias, turmas, posts, usuarios WHERE codigo='%s' AND num_turma=%d AND ano=%d AND turmas.semestre=%d AND usuarios.id_usuario=posts.id_usuario AND posts.id_turma=turmas.id_turma AND turmas.id_materia=materias.id_materia ORDER BY data_postagem DESC", addslashes($_POST["codigo"]), $_POST["turma"], $_POST["ano"], $_POST["semestre"]));
						echo sprintf("<a id=\"novo_post\" href=\"#novopost/%d/%d/%'02d/%s\">Novo post</a>", $_POST["ano"], $_POST["semestre"], $_POST["turma"], $_POST["codigo"]);
						if (pg_num_rows($result) == 0) {
							echo "Nenhum post foi feito nesta turma. Seja o primeiro!";
						}
						else {
							echo "<ul id=\"posts\">";
							while (($row = pg_fetch_array($result))) {
								echo sprintf("<li><a href=\"#turma/%d/%d/%'02d/%s/%s\"><div>%s<span>&raquo; por <b>%s</b> em <b>%s</b></span></div><p><strong>%d</strong><br/> visualiza&ccedil;&otilde;es</p></a></li>", $_POST["ano"], $_POST["semestre"], $_POST["turma"], $_POST["codigo"], $row["link"], $row["titulo"], $row["nome"], date("d/m/Y", strtotime($row["data_postagem"])), $row["visualizacoes"]);
							}
							echo "</ul>";
						}
					}
					break;
					
				case "request_novo_post":
					if ($isloged && isset($_POST["codigo"]) && is_numeric($_POST["turma"]) && is_numeric($_POST["ano"]) && is_numeric($_POST["semestre"])) {
						$result = pg_query($dbcon, sprintf("SELECT id_turma FROM materias, turmas WHERE codigo='%s' AND num_turma=%d AND ano=%d AND turmas.semestre=%d AND turmas.id_materia=materias.id_materia", addslashes($_POST["codigo"]), $_POST["turma"], $_POST["ano"], $_POST["semestre"]));
						if (pg_num_rows($result) == 1) {
							$row = pg_fetch_array($result);
							?>
							<div id="post">
								<input onclick="postar(<?php echo $row["id_turma"]; ?>);" value="Postar" type="button" id="postar" />
								<label>T&iacute;tulo:</label>
								<input name="titulo" type="text" id="titulo" />
								
								<label>Texto:</label>
								<textarea id="texto" name="texto" rows="15" cols="80" style="width: 100%;" class="tinymce"></textarea>
							</div>
							<?php
						}
						else {
							echo "Ocorreu um erro inesperado.";
						}
					}
					break;
					
				case "request_novo_curso":
					if ($isloged) {
						displayBackButton();
						
						echo "
						<div id=\"cursos_input\" class=\"input_form\">
							<h1>Cadastrar curso</h1>
							<form method=\"post\" action=\"\"id=\"cadastrar_cursos_form\" >
								<span>Nome :</span><input name=\"nome\" type=\"text\" id=\"cad_curso_nome\" class=\"input_edit\" />
								<span>Sigla :</span><input name=\"sigla\" type=\"text\" id=\"cad_curso_sigla\" class=\"input_edit\" />								
								<p id=\"cursowrong\">&nbsp;</p><input name=\"submit\" type=\"submit\" class=\"submit\" value=\"\" />
							</form>
						</div>
						";			
						
					}
					break;
					
				case "request_nova_turma":
					if ($isloged && isset($_POST["curso"]) && isset($_POST["materia"])) {
						displayBackButton();
						
						echo "
						<div id=\"turma_input\" class=\"input_form\">
							<h1>Cadastrar turma</h1>
							<form method=\"post\" action=\"\" id=\"cadastrar_turma_form\" >
								<span>N&uacute;mero :</span><input name=\"numero\" type=\"texte\" id=\"cad_turma_numero\" class=\"input_edit\" />
								<span>Ano :</span><input name=\"ano\" type=\"text\" id=\"cad_turma_ano\" class=\"input_edit\" />
								<span>Semestre :</span><select id=\"cad_turma_semestre\" class=\"input_select\"><option selected=\"selected\" value=\"1\">1</option><option value=\"2\">2</option></select>
								<p id=\"turmawrong\">&nbsp;</p><input name=\"submit\" type=\"submit\" class=\"submit\" value=\"\" />
							</form>
						</div>
						";			
						
					}
					break;
					
				case "request_add_materias":
					if ($isloged && isset($_POST["curso"])) {
						
						displayBackButton();

						echo "<a id=\"nova_materia\" href=\"#nova_materia\">Nova mat&eacute;ria</a>";
						
						$cols = array("codigo", "nome", "semestre");
														
						echo "<table class=\"materias_curso\" border=\"1\">";
						echo "<tr>";
						echo "<th>Selecionado</th>";
						for ($i = 0; $i < count($cols); $i++) {
							echo "<th>";
							echo $cols[$i];
							echo "</th>";
						}
						echo "</tr>";
						
						$result = pg_query($dbcon, "SELECT codigo, nome, semestre FROM materias ORDER BY nome");						
						$rows = pg_fetch_all($result);
						
						$count = count($rows);
						
						echo "<h2>$count mat&eacute;ria(s) cadastrada(s)</h2>";
						echo "<input onClick=\"selecionarTudo($count, true);\" value=\"Selecionar tudo\" type=\"button\"/>";
						echo "<input onClick=\"selecionarTudo($count, false);\" value=\"Desmarcar tudo\" type=\"button\"/>";
						
						for ($i = 0; $i < $count; $i++) {
						
							$row = $rows[$i];
							
							echo "<tr>";
							echo "<td><input type=\"checkbox\" id=\"id_check_$i\"></td>";
							for ($j = 0; $j < count($cols); $j++) {
								echo "<td id=\"id_$cols[$j]_$i\">";
								echo $row[$cols[$j]];
								echo "</td>";
							}
							echo "</tr>";
						}										
						
						echo "</table>";
						
						echo "<input onClick=\"adicionarMaterias($count);\" value=\"Adicionar materias\" type=\"button\"/>";
					}
					break;
				
				case "request_nova_materia":
					if ($isloged) {
						displayBackButton();
						
						echo "
						<div id=\"materias_input\" class=\"input_form\">
							<h1>Cadastrar mat&eacute;ria</h1>
							<form method=\"post\" action=\"\" id=\"cadastrar_materias_form\" >
								<span>Sigla :</span><input name=\"sigla\" type=\"text\" id=\"cad_mat_sigla\" class=\"input_edit\" />
								<span>Nome :</span><input name=\"nome\" type=\"text\" id=\"cad_mat_nome\" class=\"input_edit\" />
								<span>Semestre :</span><input name=\"semestre\" type=\"text\" id=\"cad_mat_semestre\" class=\"input_edit\" />
								<p id=\"matwrong\">&nbsp;</p><input name=\"submit\" type=\"submit\" class=\"submit\" value=\"\" />
							</form>
						</div>
						";			
						
					}
					break;
					
				case "cadastrar_curso":
					if ($isloged) {
						if (isset($_POST["nome"]) && isset($_POST["sigla"])) {
						
							$nome = $_POST["nome"];
							$sigla = strtoupper($_POST["sigla"]);
							
							$err = "";
							
							if ($sigla == "" && $nome == "") {
								$err = "Preencha todos os campos!";
							}
							else {					
								
								if ($nome == "") {
									$err = "Preencha o nome!";
								}
								
								if ($sigla == "") {
									$err .= "Preencha a sigla (Ex: SI)!<br>";
								}						
							}
							
							if ($err != "")
							{
								die($err);
							}
							else
							{
								pg_send_query($dbcon, "INSERT INTO cursos (nome, sigla) VALUES ('$nome', '$sigla')");
								
								$result = pg_get_result($dbcon);
								
								$error = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
								
								if ($error) {
								
									if ($error == $ERROR_UNIQUE_VIOLATION) {
										die("J&aacute; existe um curso com esse nome/sigla!");
									}
									else
									{
										die("Erro ao cadastrar curso!");
									}
								}
							}
							
							echo "yes";
						}
						else
							echo "Preencha todos os campos!";
					}
						
					break;
					
				case "cadastrar_turma":
					if ($isloged && isset($_POST["materia"]) && isset($_POST["numero"]) && isset($_POST["ano"]) && isset($_POST["semestre"])) {
						
							if (!is_numeric($_POST["numero"])) {
								die("N&uacute;mero da turma deve ser num&eacute;rico!");
							}
							
							if (!is_numeric($_POST["ano"])) {
								die("Ano da turma deve ser num&eacute;rico!");
							}
							
							if (!is_numeric($_POST["semestre"]) || ($_POST["semestre"] < 1 || $_POST["semestre"] > 2)) {
								die("Semestre da turma deve ser um n&uacute;mero entre 1 e 2!");
							}
							
							$materia = $_POST["materia"];
							$result = pg_query($dbcon, "SELECT id_materia FROM materias WHERE codigo='$materia'");
		
							if(pg_num_rows($result)) {
								
								$row = pg_fetch_array($result);
								
								$id_materia = $row["id_materia"];								
								$num_turma = $_POST["numero"];
								$ano = $_POST["ano"];
								$semestre = $_POST["semestre"];
								
								pg_send_query($dbcon, "INSERT INTO turmas (id_materia, num_turma, ano, semestre) VALUES ($id_materia, $num_turma, $ano, $semestre)");
									
								$result = pg_get_result($dbcon);
									
								$error = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
									
								if ($error) {
									
									die("Erro ao cadastrar turma!");

								}
								
								echo "yes";
							}
							else
								die("Erro ao cadastrar turma!");							
					}					
					break;
					
				case "cadastrar_materia":
					if ($isloged && isset($_POST["sigla"]) && isset($_POST["nome"]) && isset($_POST["semestre"])) {
						
						$sigla = $_POST["sigla"];
						$nome = $_POST["nome"];
						$semestre = $_POST["semestre"];
						
						$err = "";
						
						if ($sigla == "" && $nome == "") {
							$err = "Preencha todos os campos!";
						}
						else {					
							
							if ($sigla == "") {
								$err = "Preencha a sigla (Ex: ACH-0000)!<br>";
							}
							
							if ($nome == "") {
								$err .= "Preencha o nome!";
							}
						}
						
						if ($err != "")
						{
							die($err);
						}
						else
						{
							pg_send_query($dbcon, "INSERT INTO materias (codigo, nome, semestre) VALUES ('$sigla', '$nome', $semestre)");
							
							$result = pg_get_result($dbcon);
							
							$error = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
							
							if ($error) {
							
								if ($error == $ERROR_UNIQUE_VIOLATION) {
									die("J&aacute; existe uma mat&eacute;ria com a sigla '" . $sigla . "'");
								}
								else
									die("Erro ao cadastrar mat&eacuteria! ($error)");
							}
						}
						
						echo "yes";
					}
					else
						echo "Preencha todos os campos!";
						
					break;	
					
				case "adicionar_materia":
					if ($isloged && isset($_POST["curso"]) && isset($_POST["materia"])) {
					
						$curso = $_POST["curso"];
						$materia = $_POST["materia"];
						
						$result = pg_query($dbcon, "SELECT id_curso FROM cursos WHERE sigla='$curso'");
		
						if(pg_num_rows($result)) {
							
							$row = pg_fetch_array($result);
							
							$id_curso = $row["id_curso"];
							
							$result = pg_query($dbcon, "SELECT id_materia FROM materias WHERE codigo='$materia'");
							
							if(pg_num_rows($result)) {
							
								$row = pg_fetch_array($result);
								
								$id_materia = $row["id_materia"];
							
								pg_query($dbcon, "INSERT INTO materias_cursos (id_materia, id_curso) VALUES ($id_materia, $id_curso)");
							}
						}
						
					}
					break;
					
				case "postar":
					if ($isloged && isset($_POST["titulo"]) && isset($_POST["conteudo"]) && is_numeric($_POST["turma"])) {
						$link = toAscii($_POST["titulo"]);
						$i = 1;
						while (true) {
							$s = ($i == 1 ? "" : "-" . $i);
							$result = pg_query($dbcon, sprintf("SELECT id_post FROM posts WHERE id_turma=%d AND link='%s'", $_POST["turma"], $link . $s));
							if (pg_num_rows($result) == 0) {
								$link .= $s;
								break;
							}
							$i++;
						}
						
						$conteudo = str_replace("'", "&#39;", $_POST["conteudo"]);
						pg_query($dbcon, sprintf("INSERT INTO posts (id_turma, id_usuario, data_postagem, titulo, conteudo, link) VALUES (%d, %d, now(), '%s', '%s', '%s')", $_POST["turma"], $userid, addslashes($_POST["titulo"]), $conteudo, $link));
						pg_query($dbcon, sprintf("UPDATE turmas SET num_posts=num_posts+1 WHERE id_turma=%d", $_POST["turma"]));
						echo $link;
					}
					break;
					
				case "favoritar":
					if ($isloged && isset($_POST["id_usuario"]) && isset($_POST["id_turma"])) {
						
						$id_usuario = $_POST["id_usuario"];
						$id_turma = $_POST["id_turma"];
						
						pg_query($dbcon, "INSERT INTO favoritas (id_usuario, id_turma) VALUES ($id_usuario, $id_turma)");						
					}
					break;
					
				case "desfavoritar":
					if ($isloged && isset($_POST["id_usuario"]) && isset($_POST["id_turma"])) {
						
						$id_usuario = $_POST["id_usuario"];
						$id_turma = $_POST["id_turma"];					
						
						pg_query($dbcon, "DELETE FROM favoritas WHERE id_usuario=$id_usuario AND id_turma=$id_turma");						
					}
					break;
					
				case "isloged":
					echo ($isloged ? "yes" : "no");
					break;
				
			}
		}
	}
?>