<?php

	// Fonte: http://www.postgresql.org/docs/8.1/static/errcodes-appendix.html
	$ERROR_UNIQUE_VIOLATION = 23505;
	
	if (isset($_POST["action"])) {
	
		$dbcon = pg_connect("host=localhost port=5432 dbname=foreach user=postgres password=root");
		if (!$dbcon) {
			die("Error!");
		}
		
		$action = $_POST["action"];
		
		switch($action) {
			case "materia":
				if (isset($_POST["sigla"]) && isset($_POST["nome"])) {
					$sigla = $_POST["sigla"];
					$nome = $_POST["nome"];
					
					$err = "";
					
					if ($sigla == "" && $nome == "") {
						$err = "Preencha todos os campos!";
					}
					else {					
						
						if ($sigla == "") {
							$err = "Preencha a sigla (Ex: ACH-0000)!<br>";
						}
						
						if ($nome == "") {
							$err .= "Preencha o nome!";
						}
					}
					
					if ($err != "")
					{
						die($err);
					}
					else
					{
						pg_send_query($dbcon, "INSERT INTO materias (sigla, nome) VALUES ('$sigla', '$nome')");
						
						$result = pg_get_result($dbcon);
						
						$error = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
						
						if ($error) {
						
							if ($error == $ERROR_UNIQUE_VIOLATION) {
								die("J&aacute; existe uma mat&eacute;ria com a sigla '" . $sigla . "'");
							}
							else
								die("Erro ao cadastrar mat&eacuteria!");
						}
					}
					
					echo "yes";
				}
				else
					echo "Preencha todos os campos!";
					
				break;	
		}
	}
	else {
		echo "Arquivo errado compadre";
	}
?>