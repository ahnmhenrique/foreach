var actualPage = 0;
var loged = false;

/*
0 - Nada
1 - Login
2 - Registrar
3 - Cursos
4 - Localizar
5 - Sobre
6 - Cadastrar mat�ria
7 - Favoritas
8 - Turmas
9 - Posts
10 - Materias cursos
11 - Turmas materia
12 - Novo post
13 - Novo curso
14 - Nova turma
15 - Adicionar materia
16 - Nova materia
*/

function setupTinyMCE() {
	$("textarea.tinymce").tinymce({
		// Location of TinyMCE script
		script_url : 'js/tiny_mce/tiny_mce.js',

		// General options
		theme : "advanced",
		skin : "cirkuit",
		plugins : "autolink,lists,pagebreak,style,table,advimage,advlink,emotions,inlinepopups,preview,media,contextmenu,paste,directionality,xhtmlxtras",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,|,forecolor,backcolor",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image",
		theme_advanced_buttons3 : "tablecontrols,|,hr,|,sub,sup,|,charmap,emotions,|,preview",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : false,
		theme_advanced_path : false,

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js"
	});
}

function favoritar(id_usuario, id_turma) {
	$.post("engine.php", {
		action: "favoritar",
		id_usuario: id_usuario,
		id_turma: id_turma
	},
	function (data) {		
		anchorChanged();
	});
}

function desfavoritar(id_usuario, id_turma) {
	$.post("engine.php", {
		action: "desfavoritar",
		id_usuario: id_usuario,
		id_turma: id_turma
	},
	function (data) {
		anchorChanged();
	});
}

function postar(idturma) {
	hash = document.location.hash.slice(1).split("/");
	$.post("engine.php", {
		action: "postar",
		turma: idturma,
		titulo: $("#titulo").val(),
		conteudo: tinyMCE.get("texto").getContent()
	},
	function (data) {
		document.location.hash = "turma/" + hash[1] + "/" + hash[2] + "/" + hash[3] + "/" + hash[4] + "/" + data;
		anchorChanged();
	});
}

function adicionarMaterias(count) {
		
	hash = document.location.hash.slice(1).split("/");
	
	for (i = 0; i < count; i++) {
			
		if ($("#id_check_"+i).is(":checked")) {
			
			materia = $("#id_codigo_"+i).text();
			
			$.post("engine.php", {
				action: "adicionar_materia",
				curso: hash[1],
				materia: materia
			},
			function (data) {
				document.location.hash = "cursos/" + hash[1];
				anchorChanged();
			});
		}
	}
}

function selecionarTudo(count, checked) {

	for (i = 0; i < count; i++) {
		$("#id_check_"+i).attr('checked', checked);
	}
	
}

function setupCursosForm() {

	// Muda o submit do form de cadastro de cursos	
	$("#cadastrar_cursos_form").submit(function () { 		
		$.post("engine.php", {
			action: "cadastrar_curso",
			nome: $("#cad_curso_nome").val(),
			sigla: $("#cad_curso_sigla").val()
			
		},
		function (data) {
			if (data == "yes") {
				$("input").val("");
				$("#cursowrong").html("Cadastro efetuado com sucesso!").css({color: "#0A0"}).fadeIn("fast");
			}
			else {
				$("#cursos_input").effect("shake", {times: 3}, 500);
				$("#cursowrong").html(data).css({"color": "#F00"}).fadeIn();
			}
		});
	
		return false;
	});
}

function setupTurmasForm() {

	hash = document.location.hash.slice(1).split("/");	

	// Muda o submit do form de cadastro de turmas	
	$("#cadastrar_turma_form").submit(function () { 		
		$.post("engine.php", {
			action: "cadastrar_turma",
			materia: hash[2],
			numero: $("#cad_turma_numero").val(),
			ano: $("#cad_turma_ano").val(),
			semestre: $("#cad_turma_semestre").val()
		},
		function (data) {
			if (data == "yes") {
				$("input").val("");
				$("#turmawrong").html("Cadastro efetuado com sucesso!").css({color: "#0A0"}).fadeIn("fast");
			}
			else {
				alert($("#cad_turma_semestre").val());
				$("#turma_input").effect("shake", {times: 3}, 500);
				$("#turmawrong").html(data).css({"color": "#F00"}).fadeIn();
			}
		});
	
		return false;
	});
}

function setupMateriasForm() {

	// Muda o submit do form de cadastro de mat�rias
	$("#cadastrar_materias_form").submit(function () {
		$.post("engine.php", {
			action: "cadastrar_materia",
			sigla: $("#cad_mat_sigla").val(),
			nome: $("#cad_mat_nome").val(),
			semestre: $("#cad_mat_semestre").val()
		},
		function (data) {
			if (data == "yes") {
				$("input").val("");
				$("#matwrong").html("Cadastro efetuado com sucesso!").css({color: "#0A0"}).fadeIn("fast");
			}
			else {
				$("#materias_input").effect("shake", {times: 3}, 500);
				$("#matwrong").html(data).css({"color": "#F00"}).fadeIn();
			}
		});
		
		return false;
	});	
}

function loadBody(request) {
	$("#body").fadeOut("fast", function () {
		$.post("engine.php", {
			action: request
		},
		function (data) {
			$("#body").html(data).fadeIn("fast");
		});
	});
}

function loadPage() {
	$.post("engine.php", {
		action: "isloged"
	},
	function (data) {
		hash = document.location.hash.slice(1).split("/");
		if (data == "yes") {
			loged = true;
			if (hash[0] == "sair") {
				$.post("engine.php", {
					action: "logout"
				},
				function (data) {
					loged = false;
					$("#body").css({display: none});
					$("body").css({backgroundColor: "#0f80de"});
					$("#login_form").css({display: "block"});
					$("#main_input").fadeIn();
					document.location.hash = "#";
					actualPage = 1;
				});
			}
			else {
				$("#bar").css({display: "block"});
				$("body").css({backgroundColor: "#DDD"});
				
				if (hash[0] == "favoritas") {
					actualPage = 7;
					loadBody("request_favoritas");
				}
				else if (hash[0] == "novopost") {
					actualPage = 12;
					$.post("engine.php", {
							action: "request_novo_post",
							ano: hash[1],
							semestre: hash[2],
							turma: hash[3],
							codigo: hash[4]
						},
						function (data) {
							$("#body").html(data);
							setupTinyMCE();
						});
				}
				else if (hash[0] == "turma") {
					if (hash[5] == null) {
						actualPage = 8;
						$.post("engine.php", {
							action: "request_turma",
							ano: hash[1],
							semestre: hash[2],
							turma: hash[3],
							codigo: hash[4]
						},
						function (data) {
							$("#body").html(data);
						});
					}
					else {
						actualPage = 9;
						$.post("engine.php", {
							action: "request_post",
							ano: hash[1],
							semestre: hash[2],
							turma: hash[3],
							codigo: hash[4],
							link: hash[5]
						},
						function (data) {
							$("#body").html(data);
						});
					}
				}
				else if (hash[0] == "cursos") {
					if (hash[1] == null) {
						actualPage = 3;
						loadBody("request_cursos");
					}
					else {
						if (hash[2] == null) {
							actualPage = 10;
							$("#body").fadeOut("fast", function () {
								$.post("engine.php", {
									action: "request_materias",
									sigla: hash[1]
								},
								function (data) {
									$("#body").html(data).fadeIn("fast");
								});
							});
						}
						else {
							actualPage = 11;
							$.post("engine.php", {
								action: "request_turmas_materia",
								sigla: hash[1],
								codigo: hash[2]
							},
							function (data) {
								$("#body").html(data);
							});
						}
					}
				}
				else {
					document.location.hash = "#cursos";
					actualPage = 3;
					loadBody("request_cursos");
				}
			}
		}
		else {
			loged = false;
			$("#body").css({display: "none"});
			$("body").css({backgroundColor: "#0f80de"});
			
			if (hash[0] == "registrar") {
				$("#register_form").css({display: "block"});
				actualPage = 2;
			}
			else {
				$("#login_form").css({display: "block"});
				actualPage = 1;
			}
			
			$("#main_input").fadeIn();
		}
	});
}

function anchorChanged() {
	hash = document.location.hash.slice(1).split("/");	
	
	if (loged) {
		if (hash[0] == "sair") {			
			$.post("engine.php", {
				action: "logout"
			},
			function (data) {
				loged = false;
				if (actualPage > 2) {
					$("#body").fadeOut();
					$("#bar").slideUp();
					$("body").css({backgroundColor: "#0f80de"});
					$("#login_form").css({display: "block"});
					$("#main_input").fadeIn();
				}
				
				document.location.hash = "#";
				actualPage = 1;
			});
		}
		else {
			if (actualPage < 3) {
				$("#main_input").fadeOut();
				$("#bar").slideDown();
				$("body").css({backgroundColor: "#DDD"});
				actualPage = 3;
			}		
			
			if (hash[0] == "cadastrar_materias") {
				$("input").val("");
				$("#cadastrar_materias_form").css({display: "block"});
				$("#materias_input").fadeIn();
				
				actualPage = 6;
			}
			else if (hash[0] == "cursos") {
				if (hash[1] == null) {
					actualPage = 3;
					loadBody("request_cursos");
				}
				else {
					if (hash[2] == null) {
						actualPage = 10;
						$("#body").fadeOut("fast", function () {
							$.post("engine.php", {
								action: "request_materias",
								sigla: hash[1]
							},
							function (data) {
								$("#body").html(data).fadeIn("fast");
							});
						});
					}
					else {
						actualPage = 11;
						$("#body").fadeOut("fast", function () {
							$.post("engine.php", {
								action: "request_turmas_materia",
								sigla: hash[1],
								codigo: hash[2]
							},
							function (data) {
								$("#body").html(data).fadeIn("fast");
							});
						});
					}
				}
			}
			else if (hash[0] == "favoritas") {
				actualPage = 7;
				loadBody("request_favoritas");
			}
			else if (hash[0] == "turma") {
				if (hash[5] == null) {
					actualPage = 8;
					$("#body").fadeOut("fast", function () {
						$.post("engine.php", {
							action: "request_turma",
							ano: hash[1],
							semestre: hash[2],
							turma: hash[3],
							codigo: hash[4]
						},
						function (data) {
							$("#body").html(data).fadeIn("fast");
						});
					});
				}
				else {
					actualPage = 9;
					$("#body").fadeOut("fast", function () {
						$.post("engine.php", {
							action: "request_post",
							ano: hash[1],
							semestre: hash[2],
							turma: hash[3],
							codigo: hash[4],
							link: hash[5]
						},
						function (data) {
							$("#body").html(data).fadeIn("fast");
						});
					});
				}
			}
			else if (hash[0] == "novopost") {
				actualPage = 12;
				$("#body").fadeOut("fast", function () {
					$.post("engine.php", {
						action: "request_novo_post",
						ano: hash[1],
						semestre: hash[2],
						turma: hash[3],
						codigo: hash[4]
					},
					function (data) {
						$("#body").html(data).fadeIn("fast");
						setupTinyMCE();
					});
				});
			}
			else if (hash[0] == "novocurso") {
				actualPage = 13;
				
				$("#body").fadeOut("fast", function () {
					$.post("engine.php", {
						action: "request_novo_curso"
					},
					function (data) {
						$("#body").html(data).fadeIn("fast");
						setupCursosForm();
					});
				});
			}
			else if (hash[0] == "novaturma") {
				actualPage = 14;
				
				$("#body").fadeOut("fast", function () {
					$.post("engine.php", {
						action: "request_nova_turma",
						curso: hash[1],
						materia: hash[2]
					},
					function (data) {
						$("#body").html(data).fadeIn("fast");
						setupTurmasForm();
					});
				});
			}
			else if (hash[0] == "addmateria") {
				actualPage = 15;
				
				$("#body").fadeOut("fast", function () {
					$.post("engine.php", {
						action: "request_add_materias",
						curso: hash[1]
					},
					function (data) {
						$("#body").html(data).fadeIn("fast");
					});
				});
			}
			else if (hash[0] == "nova_materia") {
				actualPage = 16;
				
				$("#body").fadeOut("fast", function () {
					$.post("engine.php", {
						action: "request_nova_materia"
					},
					function (data) {
						$("#body").html(data).fadeIn("fast");
						setupMateriasForm();
					});
				});
			}
			else if (hash[0] == "sobre") {
				actualPage = 5;
				loadBody("request_sobre");
			}
		}
	}
	else {
		if (hash == "registrar" && actualPage == 1) {
			$("#login_form").slideUp();
			$("#register_form").slideDown();
			actualPage = 2;
		}
		else if (hash == "" && actualPage == 2) {
			$("#register_form").slideUp();
			$("#login_form").slideDown();
			actualPage = 1;
		}
	}
}

$(window).bind("hashchange", function () {
	anchorChanged();	
});

$(document).ready(function () {

	// Quando se d� F5, volta a pagina inicial
	//document.location.hash = "#";
	
	// Muda o submit do form de login
	$("#login_form").submit(function () {
		$.post("engine.php", {
			action: "login",
			email: $("#loginuser").val(),
			senha: $("#loginpass").val()
		},
		function (data) {
			if (data == "yes") {
				$("#loginwrong").css({display: "none"});
				$("input").val("");
				
				loged = true;
				document.location.hash = "#cursos";
				anchorChanged();
			}
			else {
				$("#main_input").effect("shake", {times: 3}, 500);
				$("#loginwrong").text("senha incorreta!").css({"color": "#F00"}).fadeIn();
			}
		});
		
		return false;
	});
	
	// Muda o submit do form de registro
	var wrongmsg = "";
	$("#register_form").submit(function () {
		regname = $("#regname").val();
		if (regname.length >= 6) {
			reguser = $("#reguser").val();
			if (reguser.length >= 5) {
				regpass = $("#regpass").val();
				if (regpass == $("#regrepass").val()) {
					if (regpass.length >= 6) {
						$.post("engine.php", {
							action: "register",
							nome: regname,
							email: reguser,
							senha: regpass
						},
						function (data) {
							if (data == "no") {
								$("#main_input").effect("shake", {times: 3}, 500);
								$("#regwrong").text("e-mail ja existente!").fadeIn();
							}
							else if (data == "yes") {
								$("#loginwrong").text("foi enviado um e-mail de confirmacao!").css({color: "#0A0"}).fadeIn();
								document.location.hash = "#";
								anchorChanged();
								$("#register_form input").val("");
							}
						});
					}
					else {
						wrongmsg = "senha deve ter 6 ou mais caracteres!";
					}
				}
				else {
					wrongmsg = "senhas diferentes!";
				}
			}
			else {
				wrongmsg = "e-mail deve ter 5 ou mais caracteres!";
			}
		}
		else {
			wrongmsg = "nome deve ter 6 ou mais caracteres!";
		}
		
		if (wrongmsg != "") {
			$("#main_input").effect("shake", {times: 3}, 500);
			$("#regwrong").text(wrongmsg).fadeIn();
		}
		return false;
	});
		
	loadPage();
});